//
//  PlantModel.swift
//  PlantActivityTracker
//
//  Created by rafiul hasan on 5/18/21.
//

import Foundation

struct Plants: Identifiable {
    var id = UUID()
    var title: String
    var image: String
}
