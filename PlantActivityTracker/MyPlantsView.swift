//
//  MyPlantsView.swift
//  PlantActivityTracker
//
//  Created by rafiul hasan on 5/18/21.
//

import SwiftUI

struct MyPlantsView: View {
    //MARK: Properties
    @State var searchText = ""
    @State var isActive: Bool = false
    
    //MARK: Demo Data
    var plants = [
        Plants(title: "easter lily", image: "1"),
        Plants(title: "elephant ears", image: "2"),
        Plants(title: "elm trees", image: "3")
    ]
    
    //MARK: Body
    var body: some View {
        VStack {
            SearchBar(text: $searchText)
            
            List(plants.filter({ "\($0)".contains(searchText.lowercased()) || searchText.isEmpty })){ plants in
                NavigationLink(
                    destination: DetailedView(rootIsActive: self.$isActive),
                    isActive: self.$isActive,
                    label: {
                        Image(plants.image)
                            .resizable()
                            .frame(width: 60, height: 60)
                            .cornerRadius(15)
                        
                        Text(plants.title.capitalized)
                    })
            }
        }
        .navigationTitle("My Plants")
    }
}

struct MyPlantsView_Previews: PreviewProvider {
    static var previews: some View {
        MyPlantsView()
    }
}
