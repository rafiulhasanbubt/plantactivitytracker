//
//  PlantActivityTrackerApp.swift
//  PlantActivityTracker
//
//  Created by rafiul hasan on 5/18/21.
//

import SwiftUI

@main
struct PlantActivityTrackerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
