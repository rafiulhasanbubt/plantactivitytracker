//
//  CustomShape.swift
//  PlantActivityTracker
//
//  Created by rafiul hasan on 5/19/21.
//

import Foundation
import SwiftUI

struct CustomShape: Shape {
    var leftCorner: UIRectCorner
    var rightCorner: UIRectCorner
    var radius: CGFloat
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [leftCorner, rightCorner], cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
